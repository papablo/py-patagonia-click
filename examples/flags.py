import click


@click.command()
@click.option('--lang', default='en', help='language of the greeting')
def hello(lang):

    if lang == 'en':
        click.echo("Hello")
    elif lang == 'es':
        click.echo("Hola")
    elif lang == 'ru':
        click.echo('Привет')
    elif lang == 'fr':
        click.echo('Salut')


if __name__ == "__main__":
    hello()
