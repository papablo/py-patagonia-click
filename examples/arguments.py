import click

langs = {
    'es': 'Hola',
    'en': 'Hello',
    'ru': 'Привет',
    'fr': 'Salut'
}


@click.command()
@click.argument('name')
@click.option('--lang', default='en', help='language of the greeting')
def hello(name, lang):
    click.echo(f'{langs[lang]}, {name}!')

if __name__ == "__main__":
    hello()
