
import click

langs = {
    'es': {
        'welcome': 'Hola',
        'farewell': 'Adios',
    },
    'en': {
        'welcome': 'Hello',
        'farewell': 'Bye'
    },
    'ru': {
        'welcome': 'Привет',
        'farewell': 'прощание'
    },
    'fr': {
        'welcome': 'Salut',
        'farewell': 'Adieu'
    }
}


@click.group()
@click.option('--lang', default='en', help='Language to use')
@click.argument('name')
@click.pass_context
def main(ctx, lang, name):
    ctx.ensure_object(dict)
    ctx.obj['lang'] = lang
    ctx.obj['name'] = name


def generic_greet(ctx, greet_type):
    lang = ctx['lang']
    name = ctx['name']

    return f"{langs[lang][greet_type]}, {name}"


@main.command(help='Say a welcome')
@click.option('--mood', type=click.Choice(['happy', 'sad']), required=False)
@click.pass_obj
def welcome(ctx, mood):
    mood = ':)' if mood == 'happy' else ':('
    click.echo(
        f"{ generic_greet(ctx, 'welcome') } {mood}"
    )


@main.command(help='Bid farewell')
@click.option('--mood', type=click.Choice(['sad', 'ironic']), required=False)
@click.pass_obj
def farewell(ctx, mood):
    mood = ':(' if mood == 'sad' else 'xD'
    click.echo(
        f"{generic_greet(ctx, 'farewell')} {mood}"
    )


if __name__ == "__main__":
    main()
