import click


@click.command()
def hello_with_prompt():
    name = click.prompt('Insert your name', type=str)

    click.echo(f"Hello, {name}")


@click.command()
def hello_with_pass():
    name = click.prompt('Insert your name', type=str)
    pwd = click.prompt(f'Password for {name}', type=str, hide_input=True)

    if pwd == 'asdf1234':
        click.echo(f"Hello, {name}")
    else:
        click.echo(f"Login failed for {name}")


if __name__ == "__main__":
    hello_with_pass()
